# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady — Development tasks
# :Created:   gio 30 giu 2022, 8:25:34
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2022, 2023, 2024 Lele Gaifax
#

set dotenv-load := false

package-name := `python -c "from tomllib import load;print(load(open('pyproject.toml', 'rb'))['project']['name'])"`
package-version := `bump-my-version show current_version`
dist-file-name := replace(package-name, '.', '_') + "-" + package-version

# List available targets
@_help:
  just --list

tests_dir := "tests"

# Run tests
check *cases:
  #!/usr/bin/env python3
  from os import environ
  from pathlib import Path
  from subprocess import PIPE, STDOUT, run
  from sys import exit

  tests_dir = Path('{{tests_dir}}')

  if '{{cases}}':
      cases = (tests_dir / case for case in '{{cases}}'.split())
  else:
      cases = (case for case in tests_dir.iterdir()
               if case.is_dir() and (case / 'test.sh').exists())

  try:
      for case in sorted(cases):
          print(f'{case}: ', end='')
          result = run(('bash', case / 'test.sh'),
                       env=environ | {'LC_ALL': 'C', 'SQLALCHEMY_SILENCE_UBER_WARNING': '1'},
                       stderr=STDOUT, stdout=PIPE)
          if result.returncode != 0:
              print('ERROR, output follows:')
              print(result.stdout.decode('utf-8'))
              break
          else:
              print('OK')
      run(('bash', tests_dir / 'postgresql', 'stop'))
  except KeyboardInterrupt:
      exit(1)


#################
# RELEASE TASKS #
#################

release-hint := '''
  >>>
  >>> Do your duties (update CHANGES.rst for example), then
  >>> execute “just tag-release”.
  >>>
'''

# Release new major/minor/dev version
release *kind='minor':
    bump-my-version bump {{kind}}
    @echo '{{release-hint}}'

# Assert presence of release timestamp in CHANGES.rst
_check-release-date:
    @fgrep -q "{{package-version}} ({{`date --iso-8601`}})" CHANGES.rst \
      || (echo "ERROR: release date of version {{package-version}} not set in CHANGES.rst"; exit 1)

# Assert that everything has been committed
_assert-clean-tree:
    @test -z "`git status -s --untracked=no`" || (echo "UNCOMMITTED STUFF" && false)

# Check dist metadata
_check-dist:
    pyproject-build --sdist
    twine check dist/{{dist-file-name}}.tar.gz

# Tag new version
tag-release: _check-release-date _check-dist
    git commit -a -m "Release {{package-version}}"
    git tag -a -m "Version {{package-version}}" v{{package-version}}

# Build sdist and wheel
build: _assert-clean-tree
    pyproject-build

# Build and upload to [Test]PyPI
upload *repo='pypi': build
    twine upload --repository={{repo}} dist/{{dist-file-name}}.tar.gz dist/{{dist-file-name}}*.whl

# Upload to PyPI and push commits and tag to Gitlab
publish: upload
    git push
    git push --tags
