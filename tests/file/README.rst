.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- File test notes
.. :Created:   dom 08 apr 2018 13:54:36 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2018 Lele Gaifax
..

=================
 File test notes
=================

This test exercises the ``!File`` tag.
