# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady -- File package
# :Created:   dom 08 apr 2018 13:59:31 CEST
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2018 Lele Gaifax
#

from setuptools import setup

setup(name='testdbloady',
      version='1.0',
      py_modules=['model'])
