.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- Hstore test notes
.. :Created:   lun 07 nov 2016 10:21:12 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

===================
 Hstore test notes
===================

This test verifies that the loader understands the notation used to populate a PostgreSQL's
``HSTORE`` field.
