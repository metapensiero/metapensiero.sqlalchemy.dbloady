.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- Json test notes
.. :Created:   mar 08 nov 2016 09:54:03 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

=================
 Json test notes
=================

The test checks whether dbloady groks PostgreSQL's ``JSON`` fields.
