.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- M2M test notes
.. :Created:   sab 18 gen 2020, 08:37:57
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020 Lele Gaifax
..

=========================
 Many to Many test notes
=========================

Tests whether the loader is able to deal with `Many to Many relationships`__.

__ https://docs.sqlalchemy.org/en/13/orm/basic_relationships.html#many-to-many
