# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady -- Non-relationship attributes test
# :Created:   lun 07 nov 2016 10:46:15 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from setuptools import setup

setup(name='testdbloady',
      version='1.0',
      py_modules=['model'])
