.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- Property based attributes test notes
.. :Created:   mer 07 giu 2017 14:12:56 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2017 Lele Gaifax
..

======================================
 Property based attributes test notes
======================================

Tests whether the loader is able to deal with `SQL Expressions as Mapped Attributes`__.

__ http://docs.sqlalchemy.org/en/rel_1_1/orm/mapped_sql_expr.html
