.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- Raw SQL test notes
.. :Created:   mar 15 nov 2016 14:13:01 CET
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2016 Lele Gaifax
..

====================
 Raw SQL test notes
====================

This simple test exercises the raw sql feature.
