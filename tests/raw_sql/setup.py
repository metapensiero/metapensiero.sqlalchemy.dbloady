# -*- coding: utf-8 -*-
# :Project:   metapensiero.sqlalchemy.dbloady -- Test package
# :Created:   mar 15 nov 2016 14:19:01 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2016 Lele Gaifax
#

from setuptools import setup

setup(name='testdbloady',
      version='1.0',
      py_modules=['model'])
