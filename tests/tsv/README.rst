.. -*- coding: utf-8 -*-
.. :Project:   metapensiero.sqlalchemy.dbloady -- TSV test notes
.. :Created:   lun 24 giu 2019 18:26:52 CEST
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2019 Lele Gaifax
..

================
 TSV test notes
================

This test exercises the ``!TSV`` tag.
